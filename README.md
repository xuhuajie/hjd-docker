# 项目 #

游戏平台数据中心建设（应用docker化）.使用swarm实现HA

### 项目内容 ###

* 存放dockerfile文件
* 已开房项目中的friends-game-entry-pdk为例
* [项目文档](https://docs.google.com/document/d/1nMOp26FRV28X8Dp93HHQdxhAPFOgs00o-1EIU4W0nmw/edit#)

### 项目环境###

* 内网测试服务器
* 192.168.0.16（root,123456）
* 192.168.0.17（root,123456）
* 192.168.0.18（root,123456）
* docker engine版本1.12
* 测试目录/home/docker-data/dockerfile/dockerapp

### 项目实施 ###

* 把datomic项目中的应用docker化；
* 结合swarm实现HA；
* 共设定三个里程碑

### 单节点下应用启动方式 ###

* docker run -d --name 80166-friends-pdk -p 8046:8046 -p 8047:8047 -p 9941:9941 80166/friends-game-entry-pdk

### swarm集群启动方式 ###

* 首先swarm集群部署参考[官方文档](https://docs.docker.com/engine/swarm/swarm-mode/)

* 在集群下，启动一个容器：docker service create --network net1 -p 8046:8046 -p 8047:8047 -p 9941:9941 --name pdk-fri 80166/friends-game-entry-pdk,该容器会调度到一个node上启动，同时每个node都会监听应用的ip和端口；


